package ug.ics.demo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import lotus.domino.Database;
import lotus.domino.Document;
import lotus.domino.NotesException;
import lotus.domino.View;
import lotus.domino.ViewEntry;
import lotus.domino.ViewNavigator;

import com.ibm.xsp.extlib.util.ExtLibUtil;

public class DocumentArticle extends AbstractArticle {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	HashMap<String, Integer> categories;

	public DocumentArticle() {
		try {
			Database db = ExtLibUtil.getCurrentDatabase();
			Document pdoc=db.getProfileDocument("config","");
			
			categories=new HashMap<String, Integer>();
			initialize(pdoc);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public DocumentArticle(Document pdoc){
		try {
			categories=new HashMap<String, Integer>();
			initialize(pdoc);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	private void initialize(Document pdoc) throws NotesException {
		Vector keys=pdoc.getItemValue("categories");
		Vector days=pdoc.getItemValue("days");
		
		for(int i=0;i<keys.size();i++){
			categories.put((String) keys.get(i), ((Double)days.get(i)).intValue());
		}
		
	}

	@Override
	public Integer getExpirationDays(String categoryName) {
		if (categories.containsKey(categoryName)){
			return categories.get(categoryName);
		}
		return 100;
	}

	public List<String> getCategories() {
		ArrayList<String> res=new ArrayList<String>();
		res.addAll(categories.keySet());
		return res;
	}

}
