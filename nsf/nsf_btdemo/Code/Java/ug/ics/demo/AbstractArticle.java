package ug.ics.demo;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.faces.context.FacesContext;

import lotus.domino.NotesException;

import com.ibm.xsp.extlib.util.ExtLibUtil;
import com.ibm.xsp.model.domino.wrapped.DominoDocument;

public abstract class AbstractArticle implements ArticleController,Serializable {

	protected DominoDocument getDoc() {
		return (DominoDocument)ExtLibUtil.resolveVariable(FacesContext.getCurrentInstance(), "doc");
	}
	
	public void updateExpiration() throws NotesException {
		DominoDocument doc=getDoc();
		Calendar c=new GregorianCalendar();
		
		Integer d=getExpirationDays(doc.getItemValueString("Category"));
		
		c.add(Calendar.DATE, d);
		
		
		doc.replaceItemValue("expDate", c.getTime());

	}
	
	public abstract Integer getExpirationDays(String categoryName);

}
