package ug.ics.demo;

import java.util.List;

import lotus.domino.NotesException;


public interface ArticleController {
	
	
	public void updateExpiration() throws NotesException;
	
	
	public List<String> getCategories();
	

}
