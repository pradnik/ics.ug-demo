package ug.ics.demo;

import java.util.ArrayList;

public class SimpleArticle extends AbstractArticle {

	private static final long serialVersionUID = 1L;

	@Override
	public Integer getExpirationDays(String categoryName) {
		if (categoryName.equals("info")){
			return 1;
		}
		
		if (categoryName.equals("news")){
			return 7;
		}
		
		if (categoryName.equals("article")){
			return 30;
		}
		
		return 100;
	}

	public ArrayList<String> getCategories() {
		ArrayList<String> res=new ArrayList<String>();
		res.add("info");
		res.add("news");
		res.add("article");
		return res; 
	}
	
	
	

}
