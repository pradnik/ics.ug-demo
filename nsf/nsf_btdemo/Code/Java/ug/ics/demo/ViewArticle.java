package ug.ics.demo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import lotus.domino.Database;
import lotus.domino.View;
import lotus.domino.ViewEntry;
import lotus.domino.ViewNavigator;

import com.ibm.xsp.extlib.util.ExtLibUtil;

public class ViewArticle extends AbstractArticle {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	HashMap<String, Integer> categories;

	public ViewArticle() {
		try {
			Database db = ExtLibUtil.getCurrentDatabase();
			View v = db.getView("categories");
			ViewNavigator vnav = v.createViewNav();
			ViewEntry ve=vnav.getFirst();
			categories=new HashMap<String, Integer>();
			
			while(ve!=null){
				Vector vals=ve.getColumnValues();
				categories.put((String)vals.get(0), ((Double)vals.get(1)).intValue());
				
				ViewEntry tmpe=vnav.getNext();
				ve.recycle();
				ve=tmpe;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public Integer getExpirationDays(String categoryName) {
		if (categories.containsKey(categoryName)){
			return categories.get(categoryName);
		}
		return 100;
	}

	public List<String> getCategories() {
		ArrayList<String> res=new ArrayList<String>();
		res.addAll(categories.keySet());
		return res;
	}

}
