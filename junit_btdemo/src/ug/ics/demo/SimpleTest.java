package ug.ics.demo;

import org.junit.Test;
import static org.junit.Assert.*;


public class SimpleTest {
	
	@Test
	public void emptyInputReturnsDefault(){
		SimpleArticle sc=new SimpleArticle();
		assertEquals(100, sc.getExpirationDays("").longValue());
	}
	
	@Test
	public void infoReturnsOne(){
		SimpleArticle sc=new SimpleArticle();
		assertEquals(1, sc.getExpirationDays("info").longValue());
	}
}
