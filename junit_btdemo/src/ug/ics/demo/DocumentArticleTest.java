package ug.ics.demo;
import static org.easymock.EasyMock.*;

import java.util.Vector;

import lotus.domino.Document;
import lotus.domino.NotesException;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;


public class DocumentArticleTest {
	AbstractArticle ac;
	
	@Before
	public void setUp() throws NotesException{
		Document doc=createNiceMock(Document.class);
		Vector<String> catV=new Vector<String>();
		catV.add("news");
		catV.add("info");
		
		Vector<Double> dayV=new Vector<Double>();
		dayV.add(7.0);
		dayV.add(1.0);
		
		expect(doc.getItemValue("categories")).andReturn(catV);
		expect(doc.getItemValue("days")).andReturn(dayV);
		replay(doc);
		ac=new DocumentArticle(doc);
	}
	
	
	@Test
	public void emptyInputReturnsDefault(){
		assertEquals(100, ac.getExpirationDays("").longValue());
	}
	
	@Test
	public void infoReturnsOne(){
		assertEquals(1, ac.getExpirationDays("info").longValue());
	}
}
