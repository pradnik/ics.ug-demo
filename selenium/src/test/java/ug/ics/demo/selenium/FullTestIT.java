package ug.ics.demo.selenium;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;

import org.junit.*;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.Select;

public class FullTestIT {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
	  baseUrl = System.getProperty("webdriver.base.url");
		System.out.println(baseUrl);
		
		
//		File pathToBinary = new File("d:\\Firefox\\firefox.exe");
//		FirefoxBinary ffBinary = new FirefoxBinary(pathToBinary);
//		FirefoxProfile firefoxProfile = new FirefoxProfile();       
//		driver = new FirefoxDriver(ffBinary,firefoxProfile);
		System.setProperty("webdriver.gecko.driver", "c:\\Selenium\\geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testDate() throws Exception {
    driver.get(baseUrl);
    driver.findElement(By.linkText("NEW article")).click();
    new Select(driver.findElement(By.id("view:_id1:_id2:facetMiddle:comboCategory"))).selectByVisibleText("article");
    driver.findElement(By.cssSelector("option[value=\"article\"]")).click();
    
	try
        {
            Thread.sleep(2000);
         
        }
        catch(Exception e)
        {
        }
		
    String comutedDate=driver.findElement(By.xpath("//td/span[contains(@id,\"expDate\")]")).getText();
    
    
    Calendar expDate=new GregorianCalendar();
    expDate.add(Calendar.DATE, 30);
    SimpleDateFormat sdf=new SimpleDateFormat("MMM d, yyyy",Locale.US);
     
    assertEquals( sdf.format(expDate.getTime()),comutedDate);
  }
  
  @Test
  public void testArticleCreate() throws Exception {
    driver.get(baseUrl);
    driver.findElement(By.linkText("NEW article")).click();
    new Select(driver.findElement(By.id("view:_id1:_id2:facetMiddle:comboCategory"))).selectByVisibleText("article");
    driver.findElement(By.cssSelector("option[value=\"article\"]")).click();
    
    String demoText="ICSUG demo " + new Date();
    
    driver.findElement(By.id("view:_id1:_id2:facetMiddle:inputTitle")).clear();
    driver.findElement(By.id("view:_id1:_id2:facetMiddle:inputTitle")).sendKeys(demoText);
      
  
    driver.findElement(By.id("view:_id1:_id2:facetMiddle:submitButton")).click(); 
	
	try
        {
            Thread.sleep(2000);
            
            
        }
        catch(Exception e)
        {
        }
   
    String columnText=driver.findElement(By.xpath("//td/span[contains(@id,\"_id6:_internalViewText\")]")).getText();
    assertEquals(demoText,columnText);
  }
  
  
  

  @After
  public void tearDown() throws Exception {
	  driver.close();

        try
        {
            Thread.sleep(2000);
            driver.quit();
        }
        catch(Exception e)
        {
        }
    
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
