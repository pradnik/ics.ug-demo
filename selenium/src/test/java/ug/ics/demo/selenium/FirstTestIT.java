package ug.ics.demo.selenium;

import java.io.File;
import java.util.concurrent.TimeUnit;

import junit.framework.TestCase;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;

public class FirstTestIT extends TestCase {

	private WebDriver driver;
	private String baseUrl;

	public void setUp() throws Exception {
		baseUrl = System.getProperty("webdriver.base.url");
		System.out.println(baseUrl);
		
		
//		File pathToBinary = new File("d:\\Firefox\\firefox.exe");
//		FirefoxBinary ffBinary = new FirefoxBinary(pathToBinary);
//		FirefoxProfile firefoxProfile = new FirefoxProfile();       
//		driver = new FirefoxDriver(ffBinary,firefoxProfile);
		System.setProperty("webdriver.gecko.driver", "c:\\Selenium\\geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	public void testSimple() throws Exception {
		this.driver.get(baseUrl);
		assertEquals("Article database", this.driver.getTitle());
	}

	public void tearDown() throws Exception {
		 driver.close();

        try
        {
            Thread.sleep(2000);
            driver.quit();
        }
        catch(Exception e)
        {
        }
	}
}