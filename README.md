XPages Build Automation Demo project
==================

Demo repository from ICS.UG 2015 in Bremen.

Demostrates automated build of XPages application and test execution:
- Runs local JUnit tests
- Runs test in OSGi environment using Tycho
- Runs Selenium tests after copying db to a Domino server.

To run it in your environment you have to change server names in parent pom, put database from data directory to your server (it contains data for the app) and just urn mvn clean verify -P complete

For build automation I use work of:
- Maven - Christian G�demann - https://github.com/OpenNTF/BuildAndTestPattern4Xpages
- Ant - Cameron Gregor - https://github.com/camac/BuildXPages